<html>
    <head>
        <title>Inserir</title>	<!-- Titulo página -->
        <meta charset="utf-8">
        <link href="css/bootstrap.min.css" rel="stylesheet"> <!-- bootstrap -->
        <script src="js/bootstrap.min.js"></script>
    </head>

    <body>
        <div class="container"> 		<!-- container -->
            <div class="center"> 			<!-- rom -->
                <h1> Inserir Produto</h1> 		<!-- Titulo conteudo -->
                    <a href="index.php"><button type="button" class="btn btn-primary btn-lg">VOLTAR</button></a>
            </div>		<br>				<!--fim row-->
            <DIV>
			INSERT INTO TB_Produto(nome_produto, valor_produto, fk_tipo_produto) <br>
			VALUES('$nome_produto','$valor_produto', '$fk_tipo_produto'<br> <br>
            </DIV>
            <div class="row">
                <form action="inserir.php" method="post" name="form1">		<!-- formulario -->
                    <table class="table table-striped table-bordered">		<!-- tabela -->
                        <thead>
                            <tr> 
                                <td>Nome</td>									<!-- campo -->
                                <td><input type="text" name="nome_produto"></td>
                            </tr>
                            <tr> 
                                <td>Valor</td>
                                <td><input type="text" name="valor_produto"></td>
                            </tr>
                            <tr>
                            	<td>Tipo</td>
                            	<td>
	                            <select name="fk_tipo_produto">
								    <option value="">Selecionar</option>
								    <option value="1" >CERVEJA</option>
								    <option value="2" >REFRIGERANTE</option>
								    <option value="3" >AGUA</option>
								    <option value="4" >SALGADO</option>
								    <option value="5" >DRINK</option>
								    <option value="6" >NAO DEFINIDO</option>
								</select>
								</td>
							</tr>
                                <td></td>
                                <td><input type="submit" name="Submit" value="Inserir"></td>	<!-- botao -->
                            </tr>
                        </thead>
                    </table> <!-- fim tabela -->
                </form>	<!-- fim formulario -->
            </div> <!-- fim row -->
        </div> <!-- container -->

        <?php
        include_once("conexao.php"); //conexao com o banco de dados

        if (isset($_POST['Submit'])) {
            $nome_produto = mysqli_real_escape_string($mysqli, $_POST['nome_produto']);  //coleta as strings digitadas e atribui a variavel
            $valor_produto = mysqli_real_escape_string($mysqli, $_POST['valor_produto']);
            $fk_tipo_produto = mysqli_real_escape_string($mysqli, $_POST['fk_tipo_produto']);


            if (empty($nome_produto) || empty($valor_produto) || empty($fk_tipo_produto)) {  //checa se tem algum campo vazio no formulario
                if (empty($nome_produto)) {                //retorna o aviso de campo vazio
                    echo "<font color='red'> Campo Nome vazio </font><br/>";
                }
                if (empty($valor_produto)) {
                    echo "<font color='red'> Campo valor vazio </font><br/>";
                }
                if (empty($fk_tipo_produto)) {
                    echo "<font color='red'> Campo tipo vazio </font><br/>";
                }
            } else {  //se nao estiver vazio os campos, vai inserir todos dados na tabela pessoa		
                $result = mysqli_query($mysqli, "INSERT INTO TB_Produto(nome_produto, valor_produto, fk_tipo_produto) VALUES('$nome_produto','$valor_produto', '$fk_tipo_produto')");

                //echo "Informações salvas!";  //retorna uma messagem de confirmação

                header("Location:index.php");  //retorna p a pagina inicial, fica mais eficiente do que apenas o aviso de confirmação!
            }
        }
        ?>

    </body>
</html>
