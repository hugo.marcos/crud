<html>
    <head>
        <meta charset="utf-8">      <!-- Aceita caracteres especiais -->
        <link href="css/bootstrap.min.css" rel="stylesheet"> <!-- bootstrap -->
        <script src="js/bootstrap.min.js"></script>
        <title></title>     <!-- Titulo página -->
    </head>

    <body>
        <div class="container">             <!-- container -->
            <div class="center">            <!-- row -->
                <h1> VIEW</h1>   <!-- Titulo conteudo -->
                    <a href="index.php"><button type="button" class="btn btn-danger btn-lg">Produto</button></a>
            </div>      <br>                    <!-- fim row -->

            <DIV>
                CREATE VIEW vwProduto as <br>
SELECT nome_tipo_produto, nome_produto, valor_produto <br>
FROM TA_Tipo_produto<br>
INNER JOIN TB_Produto on id_tipo_produto = fk_tipo_produto<br>
ORDER BR id_tipo_produto, nome_produto asc;<br><br>
            </DIV>
            <div class="row">
                <table width='80%' class="table table-striped table-bordered">  <!-- tabela -->
                    <thead>                                                     <!-- cabeçalho -->
                        <tr>
                            <td>Tipo</td>
                            <td>Nome</td>
                            <td>Valor</td>
                        </tr>                                                   <!-- fim cabeçalho -->

                    </thead>
                    <tbody>                                                 <!-- corpo do conteudo -->
                        <?php
                        include_once("conexao.php");
                        $result = mysqli_query($mysqli, "SELECT * FROM vwProduto");
                        while ($res = mysqli_fetch_array($result)) {    //lista os resultados 
                            echo "<tr>";
                            echo "<td>" . $res['nome_tipo_produto'] . "</td>";
                            echo "<td>" . $res['nome_produto'] . "</td>";
                            echo "<td>" . $res['valor_produto'] . "</td>";
                            echo "</tr>";
                        }
                        ?>
                    </tbody>                   
                </table>              
            </div>
        </div>
    </body>
</html>
