<html>
    <head>
        <meta charset="utf-8">		<!-- Aceita caracteres especiais -->
        <link href="css/bootstrap.min.css" rel="stylesheet"> <!-- bootstrap -->
        <script src="js/bootstrap.min.js"></script>
        <title></title> 	<!-- Titulo página -->
    </head>

    <body>
        <div class="container">				<!-- container -->
            <div class="center"> 			<!-- row -->
                <h1> Produto</h1>	<!-- Titulo conteudo -->
                <a href="inserir.php"><button type="button" class="btn btn-primary btn-lg">INSERIR</button></a>
                <a href="view.php"><button type="button" class="btn btn-success btn-lg">VIEW</button></a>
            </div>		<br>					<!-- fim row -->
            <div class="row">
                <table width='80%' class="table table-striped table-bordered">	<!-- tabela -->
                    <thead>														<!-- cabeçalho -->
                        <tr>
                            <td>ID</td>
                            <td>Nome</td>
                            <td>Valor</td>
                            <td>Tipo</td>
                            <td>Opções</td>
                        </tr>													<!-- fim cabeçalho -->

                    </thead>
                    <tbody>													<!-- corpo do conteudo -->
                        <?php
                        include_once("conexao.php");
						$result = mysqli_query($mysqli, "SELECT * FROM TB_Produto ORDER BY id_produto asc");
                        while ($res = mysqli_fetch_array($result)) {    //lista os resultados 
                            echo "<tr>";
                            echo "<td>" . $res['id_produto'] . "</td>";
                            echo "<td>" . $res['nome_produto'] . "</td>";
                            echo "<td>" . $res['valor_produto'] . "</td>";
                            echo "<td>" . $res['fk_tipo_produto'] . "</td>";
							echo "<td>
							<a href=\"edit.php?id_produto=$res[id_produto]\">Editar</a> | 
							<a href=\"delete.php?id_produto=$res[id_produto]\" 
							onClick=\"return confirm('Voce tem certeza que deseja apagar?')\">Apagar</a>
							</td>";
                        }
                        ?>
                    </tbody>                   
                </table>              
            </div>
        </div>
    </body>
</html>
