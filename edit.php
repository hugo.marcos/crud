<?php
include_once("conexao.php"); //conexao com o banco

if (isset($_POST['update'])) {

    $id_produto = mysqli_real_escape_string($mysqli, $_POST['id_produto']);
    $nome_produto = mysqli_real_escape_string($mysqli, $_POST['nome_produto']);
    $valor_produto = mysqli_real_escape_string($mysqli, $_POST['valor_produto']);
    $fk_tipo_produto = mysqli_real_escape_string($mysqli, $_POST['fk_tipo_produto']);

    if (empty($nome_produto) || empty($valor_produto) || empty($fk_tipo_produto)){ //checa se tem algum campo vazio no formulario
        if (empty($nome_produto)) {              //retorna o aviso de campo vazio
            echo " Campo Nome vazio <br/>";
        }
        if (empty($valor_produto)) {
            echo " Campo valor vazio <br/>";
        }
        if (empty($fk_tipo_produto)) {
            echo " Campo tipo vazio <br/>";
        }
    } else { //caso nao esteja vazio, o conteúdo das novas strings serao gravados. 
        $result = mysqli_query($mysqli, "UPDATE TB_Produto SET nome_produto='$nome_produto',valor_produto='$valor_produto',fk_tipo_produto='$fk_tipo_produto' WHERE id_produto=$id_produto");
        //echo "Alterações feitas!"
        header("Location: index.php"); // retorna para a página principal
    }
}
?>
<?php
$id_produto = $_GET['id_produto']; //pega o valor do id a ser editado
$result = mysqli_query($mysqli, "SELECT * FROM TB_Produto WHERE id_produto=$id_produto"); //seleciona todos os dados do id

while ($res = mysqli_fetch_array($result)) { //mostra os dados do id
    $nome_produto = $res['nome_produto'];
    $valor_produto = $res['valor_produto'];
    $fk_tipo_produto = $res['fk_tipo_produto'];
}
?>
<html>
    <head>
        <title>Atualizar</title>		<!-- Titulo pagina -->
        <meta charset="utf-8">
        <link href="css/bootstrap.min.css" rel="stylesheet"> <!-- bootstrap -->
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container"> 		<!-- container -->
            <div class="center"> 			<!-- row -->
                <h1> Inserir </h1> 		<!-- Titulo conteudo -->
            </div>						<!-- fim row -->
            <div class="row">			<!-- row -->
                <p>
                    <a href="index.php"><button type="button" class="btn btn-primary btn-lg">VOLTAR</button></a>
                </p>
            </div>						<!-- fim row -->
            <div>
            	UPDATE TB_Produto SET nome_produto='$nome_produto',valor_produto='$valor_produto',fk_tipo_produto='$fk_tipo_produto'<br>
            	WHERE id_produto=$id_produto <br>
            </div>
            <div class="row">			<!-- row -->
                <form name="form1" method="post" action="edit.php">		<!-- Formulario -->
                    <table class="table table-striped table-bordered">
                        <thead>
                        	<tr> 
                                <td>Nome</td>
                                <td><input type="text" name="nome_produto" value="<?php echo $nome_produto; ?>"></td>
                            </tr>
                            <tr> 
                                <td>Valor</td>
                                <td><input type="text" name="valor_produto" value="<?php echo $valor_produto; ?>"></td>
                            </tr>
                            <tr>
                            	<td>Tipo</td>
                            	<td>
	                            <select name="fk_tipo_produto" >
								    <option value="<?php echo $fk_tipo_produto; ?>">(<?php echo $fk_tipo_produto; ?>)</option> <!--valor e nome do campo-->
								    <option value="1" >1 - CERVEJA</option>
								    <option value="2" >2 - REFRIGERANTE</option>
								    <option value="3" >3 - AGUA</option>
								    <option value="4" >4 - SALGADO</option>
								    <option value="5" >5 - DRINK</option>
								    <option value="6" >6 - NAO DEFINIDO</option>
								</select>
								</td>
							</tr>
                            <tr>
                                <td><input type="hidden" name="id_produto" value=<?php echo $_GET['id_produto']; ?>></td> <!-- input oculto, pega o id -->
                                <td><input type="submit" name="update" value="Atualizar"></td>	<!-- botao -->
                            </tr>
                        </thead>
                    </table><!-- fim tabela-->
                </form>	<!-- fim formulario -->
            </div>	<!-- fim row -->
        </div>	<!--fim container -->
    </body>
</html>
